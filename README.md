Rakit Input
========

Simple wrapper PHP request input getter

## Contoh Penggunaan

#### Inisiasi

```php
<?php

require("tempat/si/Input.php");

$input = new Input;
```

#### get($key, $default_value=null)

Library ini otomatis merge $_POST dan $_GET, jadi untuk ambil nilai dari 2 variable itu
pakai method get

```php
<?php

$username = $input->get("username");
$password = $input->get("password");
$remember = $input->get("remember", true); // kalo nggak ada input 'remember', $remember = true

```

#### has($key)

Method has digunain untuk ngecek, inputan dengan key tersebut ada ato nggak...

```php
<?php

if($input->has("username")) {
    exit("username harus diisi");
}

```

#### hasFile($key) dan file($key)

Method `hasFile()` digunain untuk ngecek keberadaan **single** file yang diupload. Sedangkan method `file()` digunain untuk ambil `InputFile` object dari file yang di upload. Method ini akan ngembaliin nilai berupa objek `InputFile`, dimana objek itu bisa digunain untuk upload file dengan mudah.

Contoh penggunaan

form-upload.php

```html

<form action="aksi-upload.php" method="POST" enctype="multipart/form-data">
    <input type="file" name="gambarnya"/>
    <input type="submit" value="upload"/>
</form>

```

aksi-upload.php

```php
<?php

if($input->hasFile("gambarnya")) {

    $gambarnya = $input->file("gambarnya");
    $gambarnya->move("direktori/uploadnya");

}

```

#### hasMultiFiles($key) dan multiFiles($key)

Method `hasMultiFiles()` dan `multiFiles()` sama kayak method `hasFile()` dan `file()`, cuma khusus untuk
multi file upload. Method `multiFiles()` ngembaliin `array` yang isinya daftar `InputFile` yang telah diupload.

form-multifile-upload.php

```html

<form action="aksi-multifile-upload.php" method="POST" enctype="multipart/form-data">
    <input type="file" name="gambarnya[]"/>
    <input type="file" name="gambarnya[]"/>
    <input type="file" name="gambarnya[]"/>
    <input type="submit" value="upload"/>
</form>

```

aksi-multifile-upload.php

```php
<?php

if($input->hasMultiFiles("gambarnya")) {

    $files = $input->multiFiles("gambarnya");
    foreach($files as $i => $gambarnya) {
         $gambarnya->name = "gambarnya-{$i}";
         $gambarnya->move("direktori/upload");
    }

}

```
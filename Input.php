<?php

class InputFile {

    public $tmp;

    public $name;

    public $size;

    public $error;

    public $mimeType;

    public $extension;

    protected $location;

    public function __construct(array $_file)
    {
        $this->tmp = $_file['tmp_name'];
        $this->size = $_file['size'];
        $this->error = $_file['error'];
        $this->mimeType = $_file['type'];
        $this->location = $this->tmp;
        $this->name = pathinfo($_file['name'], PATHINFO_FILENAME);
        $this->extension = pathinfo($_file['name'], PATHINFO_EXTENSION);
    }

    public function getFilename()
    {
        $ext = empty($this->extension)? "" : ".".$this->extension;
        return $this->name.$ext;
    }

    public function getLocation()
    {

        return $this->location;
    }

    public function move($location)
    {
        if(!is_uploaded_file($this->tmp)) return FALSE;

        $location = rtrim($location,"/");

        if(!is_dir($location)) {
            throw new \RuntimeException("Upload directory not found", 1);
        } else if(!is_writable($location)) {
            throw new \RuntimeException("Upload directory is not writable", 2);
        }

        
        $filepath = $location."/".$this->getFilename();

        move_uploaded_file($this->tmp, $filepath);       
        
        $has_moved = !is_uploaded_file($this->tmp);

        if($has_moved) {
            $this->location = $filepath;
        } else {
            throw new \RuntimeException("
                Upload file failed because unexpected reason. 
                Maybe there is miss configuration in your php.ini settings"
            , 3);
        }
    }

    public function __toString()
    {
        return (string) file_get_contents($this->getLocation());
    }

}


class Input {

    protected $inputs = array();
    protected $files = array();

    public function __construct()
    {
        $inputs = (array) $_POST + (array) $_GET;
        $this->inputs = $inputs;

        $this->files = $_FILES;
    }

    public function has($key)
    {
        return array_key_exists($key, $this->inputs);   
    }

    public function get($key, $default = null)
    {
        return $this->has($key)? $this->inputs[$key] : $default;
    }

    public function file($key)
    {
        if(!$this->hasFile($key)) return NULL;

        return $this->makeInputFile($this->files[$key]);
    }

    public function multiFiles($key)
    {  
        if(!$this->hasMultiFiles($key)) return array();

        $input_files = array();

        $files = $this->files[$key];

        $names = $files["name"];
        $types = $files["type"];
        $temps = $files["tmp_name"];
        $errors = $files["error"];
        $sizes = $files["size"];

        foreach($temps as $i => $tmp) {
            if(empty($tmp) OR !is_uploaded_file($tmp)) continue;

            $_file = array(
                'name' => $names[$i],
                'type' => $types[$i],
                'tmp_name' => $tmp,
                'error' => $errors[$i],
                'size' => $sizes[$i]
            );

            $input_files[] = $this->makeInputFile($_file);
        }

        return $input_files;
    }

    public function hasFile($key)
    {
        $file = @$this->files[$key];
        
        if(!$file) return FALSE;

        $tmp = $file["tmp_name"];

        if(!is_string($tmp)) return FALSE;

        return is_uploaded_file($tmp);
    }

    public function hasMultiFiles($key)
    {   
        $files = @$this->files[$key];

        if(!$files) return FALSE;

        $uploaded_files = $files["tmp_name"];
        if(!is_array($uploaded_files)) return FALSE;

        foreach($uploaded_files as $tmp_file) {
            if(!empty($tmp_file) AND is_uploaded_file($tmp_file)) return TRUE;
        }

        return FALSE;
    }

    protected function makeInputFile(array $_file)
    {
        return new InputFile($_file);
    }

}

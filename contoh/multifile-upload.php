<?php ($_SERVER["REQUEST_METHOD"] === "POST") or die("access denied, invalid request");

    error_reporting(E_ALL);

    require("../Input.php");

    $input = new Input;
    
    // jika si 'filenya' ada/diupload
    if($input->hasMultiFiles("filenya")) {

        foreach($input->multiFiles("filenya") as $i => $filenya) {
            $filenya->name = "filenya-{$i}";
            $filenya->move(__DIR__);
            echo "<a href='".$filenya->getFilename()."'>".$filenya->getFilename()."</a><br/>";
        }

    } else {
        echo "Input filenya oi..";
    }

?>

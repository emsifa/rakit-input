<?php ($_SERVER["REQUEST_METHOD"] === "POST") or die("access denied, invalid request");

    require("../Input.php");

    $input = new Input;
    
    // jika si 'filenya' ada/diupload
    if($input->hasFile("filenya")) {
        
        $file = $input->file("filenya");

        // ganti namanya            
        $file->name = "nama-barunya";

        // kalo mo ganti ekstensinya
        // $file->extension = "txt";

        $file->move(__DIR__);
        
        echo "File <a href='".$file->getFilename()."' target='blank'>".$file->getFilename()."</a> berhasil di upload";      
    } else {
        echo "Input filenya oi..";
    }

?>
